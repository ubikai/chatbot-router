import os


class Config():
    root_psw = os.environ.get("MYSQL_ROOT_PASSWORD", default=None)
    hostname = os.environ.get("MYSQL_HOSTNAME", default=None)
    db_name = os.environ.get("MYSQL_DATABASE", default=None)

    SQLALCHEMY_DATABASE_URI = 'mysql://root:{}@{}/{}'.format(root_psw, hostname, db_name)
    APPLICATION_ROOT = 'app/'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    VERIFY_TOKEN = os.environ.get("VERIFY_TOKEN", default=False)


class DevelopmentConfig(Config):
    DEBUG = True  # Turns on debugging features in Flask
    TESTING = True


class ProductionConfig(Config):
    DEBUG = False  # Turns off debugging features in Flask
    TESTING = False

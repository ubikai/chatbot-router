from app.models import *
from app import db
from functools import wraps
from flask import abort, request
from subprocess import check_output
import sys
from memcache import client
import requests
import json


def dprint(*args):  # debug print
    print(*args, file=sys.stderr)


def get_container_ip(cont_name):
    out = check_output(["dig", "+short", cont_name])
    out = out.decode("utf-8")
    out = out.split('\n', 1)[0]
    return out if out is not "" else None


def platform_only(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if request.remote_addr != get_container_ip('router_platform_1'):
            print('/!\\' * 3, "REQUEST from {} BLOCKED".format(request.remote_addr))
            abort(404)
        return f(*args, **kwargs)

    return decorated_function


def get_chatbot_id(output):
    if output['entry']:
        for event in output['entry']:
            if event.get('messaging'):
                messaging = event['messaging']
                for message in messaging:
                    if message.get('recipient'):
                        return message['recipient'].get('id')

    return None


def get_page_ip(fb_id):
    # check cache
    out = client.get(fb_id)

    if out:
        print("Retrieved IP form cache", file=sys.stderr)
        return out.decode('unicode_escape')

    # check DB
    page = FaceBookPage.query.filter(FaceBookPage.fb_id == fb_id).first()
    if page:
        container = page.container
        if container:
            ip = container.ip
            client.set(fb_id, ip)
            print("Retrieved IP form DB", file=sys.stderr)
            return ip
    else:
        # create a new entry in DB
        print("Creating page entry in DB...", file=sys.stderr)
        page_name = get_page_name(fb_id)
        page = FaceBookPage(name=page_name, fb_id=fb_id)
        db.session.add(page)
        db.session.commit()
        # page.submit()
    return None


def get_page_name(fb_id):
    name = None
    url = "https://graph.facebook.com/v2.6/{}".format(fb_id)

    args = {
        "fields": "id,name",
        "access_token": "EAAep6kOLkVoBAIFGQKqYULpSCQXXKcZACHKIzPTX3RK0zLebJZAPMfToySZAB46iLR0MPIY4NnJZBaZBbNY6A1mvNdXwaZAZCRpgjXz3nrZB73AV3uOxWEBaVv8uAZCcbmZC8NfIWsxWr6ZBC39u2R8BM2nU4hAsmNV7LlEIB6JcEibGcx3FkyE5OKiRkC1gZAD0ErZCd5TK35gSYIQZDZD"
    }

    out = requests.get(url, params=args)

    print("Getting page name ", out, file=sys.stderr)
    if out:
        out = out.json()
        name = out['name']
    else:
        raw_content = out.content
        if type(raw_content) == bytes:
            raw_content = raw_content.decode('utf-8')
        out_content = json.loads(raw_content)
        if out_content.get('error'):
            if out_content['error'].get('message'):
                print("=" * 80)
                print(out_content['error']['message'])
                print("=" * 80)

    print("Page name retrieved: ", name, file=sys.stderr)
    return name


def sync_containers():
    chatbots = {}
    out = check_output(["/usr/bin/docker", "network", "inspect", "router_default"])
    out = out.decode("utf-8")
    print(out, file=sys.stderr)
    network_info = json.loads(out)
    for container in network_info[0]['Containers']:
        # print(network_info[0]['Containers'][container]['Name'].split("_"),file = sys.stderr)
        container_name = network_info[0]['Containers'][container]['Name'].split("_")
        if "bot" in container_name:
            if 'dev' in container_name:
                print(container_name, ' <--- DEV', file=sys.stderr)
                chatbots[container_name[0] + '_dev'] = network_info[0]['Containers'][container]['IPv4Address'][:-3]
            else:
                chatbots[container_name[0]] = network_info[0]['Containers'][container]['IPv4Address'][:-3]
    conts = Container.query.all()

    # mark all containers as Inactive
    for cont in conts:
        cont.status = False
    for chatbot in chatbots:
        for cont in conts:
            if cont.name == chatbot:
                if cont.ip != chatbots[chatbot]:
                    cont.ip = chatbots[chatbot]
                cont.status = True
                break
        else:
            cont = Container(name=chatbot, ip=chatbots[chatbot], status=True)
            conts.append(cont)
    # submit all changes to db
    for cont in conts:
        db.session.add(cont)
    db.session.commit()
    return conts
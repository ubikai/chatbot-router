import sys

sys.path.append("/router/src")

from memcache import client
from app.models import FaceBookPage
from datetime import datetime


# client.set('1430242947103967', '192.168.80.26')
# client.set('2095824040433165', '192.168.80.16')
# client.set('360728581404531', '192.168.80.30')

page_id = '240122053555873'

print("Start Memcache Test")
out = None
now = datetime.now()
ip = client.get(page_id)
if ip:
    out = ip.decode('unicode_escape')
print(out)
after = datetime.now() - now
print("Memcache Time:", after)

print("\nDB retrieve time")
out = None
now = datetime.now()
page = FaceBookPage.query.filter(FaceBookPage.fb_id == page_id).first()
if page:
    container = page.container
    if container:
        out = container.ip
print(out)
after = datetime.now() - now
print("DB Time:", after)

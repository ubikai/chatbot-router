import requests

Andrei = '2380818315332476'

pagina_de_test = '369038290498522'
chatbottestpage = '2095824040433165'
politician = '240122053555873'
teko = '1095381597169171'


def test_send_message(text):
    msg = {
        'object': 'page',
        'entry': [
            {
                'time': 1537619034597,
                'id': '1430242947103967',
                'messaging': [
                    {
                        'message': {
                            'text': text,
                            'seq': 541297,
                            'mid': 'aO0qxyibLPRwfMSs3rkZ-l0WLqjKOuRIY8kLw7g7o1QoBrw'
                                   'OT87Alhcrw2fvKm0qQZ5PlGzyldfvET3W3wje1g'
                        },
                        'recipient': {
                            'id': teko
                        }
                        , 'timestamp': 1537619033246,
                        'sender': {
                            'id': Andrei
                        }
                    }
                ]
            }
        ]
    }
    return requests.post("http://localhost/facebook", json=msg)


def test_localhost():
    print(requests.get("http://localhost/"))


if __name__ == "__main__":
    # from datetime import datetime
    # now = datetime.now()
    # out = test_send_message("hi")
    # after = datetime.now()
    # print((after.microsecond-now.microsecond)/1000)
    # if out:
    #     print(out.json())
    # else:
    #     print(out)
    test_send_message("x");

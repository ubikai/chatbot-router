from app import db


class BaseMixin(object):

    def submit(self):
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()

    @classmethod
    def create(cls, **kwargs):
        obj = cls(**kwargs)
        try:
            db.session.add(obj)
            db.session.commit()
        except:
            db.session.rollback()

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
        except:
            db.session.rollback()


class FaceBookPage(BaseMixin, db.Model):
    __tablename__ = 'facebook_pages'

    id = db.Column(db.Integer, primary_key=True)
    fb_id = db.Column(db.String(64), unique=True)
    name = db.Column(db.String(64, collation='utf16_romanian_ci'))
    container_id = db.Column(db.Integer, db.ForeignKey('containers.id'))
    container = db.relationship("Container", back_populates="page")

    def __repr__(self):
        return '<Facebook Page id={}, Name={}, page_id={}>'.format(self.id, self.name, self.fb_id)


class Container(BaseMixin, db.Model):
    __tablename__ = 'containers'

    id = db.Column(db.Integer, primary_key=True)
    ip = db.Column(db.String(64), unique=True)
    name = db.Column(db.String(64), unique=True)
    status = db.Column(db.Boolean, default=True)
    page = db.relationship("FaceBookPage", back_populates="container")

    def __repr__(self):
        return '<Container id={}, Name={}, IP={}, Status={}>'.format(self.id, self.name, self.ip, self.status)

from flask_api import FlaskAPI
from config import *
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import os

app = FlaskAPI(__name__)
if os.environ.get("FLASK_ENV", default=False) == 'production':
    app.config.from_object(ProductionConfig)
else:
    app.config.from_object(DevelopmentConfig)

db = SQLAlchemy(app)
migrate = Migrate(app, db)

from app import routes, models

from os.path import dirname, basename, isfile
import glob

files = glob.glob(dirname(__file__) + "/routes/*.py")
modules = [basename(f)[:-3] for f in files if isfile(f) and not f.endswith('__init__.py') and not f.endswith('helpers.py')]
for module in modules:
    exec('from app.routes.{} import *'.format(module, module))

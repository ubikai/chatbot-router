from app.routes import *
from memcache import client


@app.route("/manage_links", methods=['GET', 'POST'])
@platform_only
def manage_links():
    if request.method == 'GET':
        data = request.args
        out = {"containers": [], "fb_pages": []}
        sync_cont = sync_containers()
        # transform sql class object to dict
        if sync_cont:
            for cont in sync_cont:
                out["containers"].append({'id': cont.id, 'name': cont.name, 'ip': cont.ip, 'status': cont.status})
        if FaceBookPage.query.all():
            for page in FaceBookPage.query.all():
                out["fb_pages"].append({'id': page.id, 'fb_id': page.fb_id, 'name': page.name, 'container_id': page.container_id})
        return out
    elif request.method == 'POST':
        dprint('IN POST')
        data = request.get_json()
        page = FaceBookPage.query.get(data['page_id'])
        cont = Container.query.get(data['cont_id'])
        if cont:
            page.container = cont
            client.set(page.fb_id, page.container.ip)
        else:
            del page.container
            client.delete(page.fb_id)
        page.submit()
        return ""

from app.routes import *

from datetime import datetime
from multiprocessing import Process


@app.route("/")
def index():
    return {"STATUS": "OK"}


@app.route("/facebook", methods=['GET', 'POST'])
def receive_message():
    if request.method == 'GET':
        """Before allowing people to message your bot, Facebook has implemented a verify token
        that confirms all requests that your bot receives came from Facebook."""
        token_sent = request.args.get("hub.verify_token")
        if token_sent == app.config['VERIFY_TOKEN']:
            return request.args.get("hub.challenge")
        return {"STATUS": "OK"}
    else:
        now = datetime.now()
        # get request parameters
        output = request.get_json()
        if not output:
            return {"ERROR": 'No JSON found!'}

        print("Request Parameters:", request.get_json(), file=sys.stderr)

        # Get Facebook Page ID
        page_id = get_chatbot_id(output)
        if not page_id:
            print("cannot find recipient_id")
            return {"ERROR": 'cannot find recipient_id'}

        print("Facebook Page ID: ", page_id, file=sys.stderr)

        ip = get_page_ip(page_id)
        if ip:
            try:
                t = Process(target=send_request, args=(ip, output))
                t.start()
                print("request took: ", datetime.now() - now)
                return {"STATUS": "Request Processed"}

            except (requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError):
                print("Invalid IP", file=sys.stderr)
                return {"ERROR": "Cannot find a container for that recipient_id"}

        else:
            print("Could not find an IP for that page", file=sys.stderr)
            return {"ERROR": "Could not find an IP for that page"}


def send_request(ip, output):
    print("Sending request to: ", "http://{}/facebook".format(ip), file=sys.stderr)
    requests.post("http://{}/facebook".format(ip), json=output)

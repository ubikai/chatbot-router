#!/usr/bin/env bash
if [ -z "${DOCKER_INIT}" ]; then
    apt-get update && \
    apt-get -y install apt-transport-https \
         ca-certificates \
         curl \
         gnupg2 \
         software-properties-common && \
    curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg > /tmp/dkey; apt-key add /tmp/dkey && \
    add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
       $(lsb_release -cs) \
       stable" && \
    apt-get update && \
    apt-get -y install docker-ce
    chmod +s /usr/bin/docker
    export DOCKER_INIT=initialized
fi

if [ ! -d "/router/migrations" ]; then
  # Control will enter here if $DIRECTORY doesn't exist.
  flask db init -d /router/migrations
fi
flask db migrate -m "up" -d /router/migrations
flask db upgrade -d /router/migrations

echo "" > /router/logs/error.log
echo "" > /router/logs/access.log

if test $FLASK_ENV = 'development'; then
    flask run --host=0.0.0.0 --port=80
else
    /usr/sbin/apache2ctl -D FOREGROUND
fi

# Testing Purpose
#if test $FLASK_ENV = 'development'; then
#    echo "development"
#else
#    echo "production"
#fi
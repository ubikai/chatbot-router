from os import environ
from helpers import get_container_ip


class Config(object):
    PROJECT_NAME = environ.get("PROJECT_NAME", default=None)

    root_psw = environ.get("MYSQL_ROOT_PASSWORD", default=None)
    hostname = environ.get("MYSQL_HOSTNAME", default=None)
    db_name = environ.get("MYSQL_DATABASE", default=None)

    SQLALCHEMY_DATABASE_URI = 'mysql://root:{}@{}/{}'.format(root_psw, hostname, db_name)
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SECRET_KEY = environ.get('SECRET_KEY') or 'johnverygood'

    ROUTER_IP = get_container_ip('router_router_1')


class DevelopmentConfig(Config):
    DEBUG = True  # Turns on debugging features in Flask
    TESTING = False


class ProductionConfig(Config):
    DEBUG = False  # Turns on debugging features in Flask
    TESTING = False

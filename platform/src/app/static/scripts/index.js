/* ******** *
 * SPARRING *
 * ******** */
var sparring = {
    "data": "daily",
    "period": 0,
    change_period: function (direction){
        sparring["period"] += direction
        sparring.get();
    },
    change_data: function (type){
        sparring["data"] = type
        sparring["period"] = 0
        sparring.get();
    },
    change_selected_button: function (){
        if (sparring.data == "daily"){
            sparring.weekly_button.addClass('disabled');
            sparring.monthly_button.removeClass('disabled');
        }
        else if (sparring.data == "weekly"){
            sparring.weekly_button.removeClass('disabled');
            sparring.monthly_button.addClass('disabled');
        }
    },
    load: function (){
        sparring.weekly_button = $("#sparring_daily");
        sparring.monthly_button = $("#sparring_weekly");

        sparring.get();
    },
    get: function (){
        $.get(`/sparring?date=${sparring["data"]}&period=${sparring["period"]}`).done(function(data) {
            /* str --to-> JSON */
            data = JSON.parse(data);
            /* remove and reset canvas */
            $("#sparring_chart").remove();
            /* Change width&height from index.html too */
            $(".chart-container").append("<canvas id='sparring_chart' width='600' height='320'><canvas>");
            /* bgColor "#00e396" */
            DrawStackedBar(data, "sparring_chart")
        });
        sparring.change_selected_button()
    }
};


/* ************ *
 * SPARRING AVG *
 * ************ */
var sparring_avg = {
    "all": [],
    "data": "weekly",
    "period": "all",
    load: function (){
        sparring_avg.get();
    },
    calc: function(){
        let mo = sparring_avg['all']['data'];
        let len = 0
        var data = {
            "avg": {"value": 0, "total_mins": 0},
            "max": {"value": Math.max(...mo), "date": "???-??"},
            "min": {"value": Math.min.apply(null, mo.filter(Boolean)), "date": "???-??"}
        }

        for (var i = 0; i < mo.length; i++){
            if (mo[i] == data['max']['value'])
                data['max']['date'] = sparring_avg['all']['labels'][i]
            if (mo[i] == data['min']['value'])
                data['min']['date'] = sparring_avg['all']['labels'][i]
            if (mo[i] != 0){
                data["avg"]["total_mins"] += mo[i];
                len++;
            }
        }

        data['avg']['value'] = Number((data["avg"]["total_mins"] / len).toFixed(1))
        sparring_avg.set_data(data);
    },
    set_data: function(data){
        $("#card_max_week_title")[0].innerHTML = 'BEST SPARRING WEEK - ' + data["max"]["date"]
        $("#card_min_week_title")[0].innerHTML = 'WORST SPARRING WEEK - ' +data["min"]["date"]

        $("#card_max_week")[0].innerHTML = data["max"]["value"] + " minutes"
        $("#card_min_week")[0].innerHTML = data["min"]["value"] + " minutes"

        $("#card_avg_week")[0].innerHTML = data["avg"]["value"] + " minutes"
    },
    get: function (){
        $.get(`/sparring?date=${sparring_avg["data"]}&period=${sparring_avg["period"]}`).done(function(data) {
            data = JSON.parse(data);
            if (data["data"].length > 0) {

                data["data"] = data["data"][0]["data"];
                delete data["legend"];
                sparring_avg["all"] = data;
                sparring_avg.calc();
            }
        });
        sparring_avg["period"] -= 1;
    },
};


/* ******** *
 * FEELINGS *
 * ******** */
var feelings = {
    "period": 0,
    change_period: function (direction){
        feelings["period"] += direction
        feelings.get();
    },
    load: function (){
        feelings.get();
    },
    get: function (){
        $.get(`/feelings?period=${sparring["period"]}`).done(function(data) {
            /* str --to-> JSON */
            data = JSON.parse(data);
            DrawApexRangeHeatmap(data, "feelings_chart")
        });
    }
};


/* ********** *
 * ATTENDANCE *
 * ********** */
var attendance = {
    load: function (){
        attendance.get();
    },
    get: function(){
            $.get(`/attendance`).done(function(raw_data) {
            let data = JSON.parse(raw_data);
            /* console.log('Attendance Loaded'); */
            /* console.log(raw_data); */

            if (data['values']['goal'] === 0){
                $("#att_text").text('No Goal Set');
            }
            else {
                $("#att_text").text(data['values']['current'] + '/' + data['values']['goal']);
                $("#myBar").css("width", data['values']['percent'] + '%');
                $("#mybar").css("color", 'red');
            }

            $("#total_att_text").text(data['values']['total']);

            $("myProgress").children();
        });
    }
};


/* ********** *
 * SKILLS *
 * ********** */
var skills = {
    "data": "daily",
    "period": 0,
    change_period: function (direction){
        skills["period"] += direction
        skills.get();
    },
    change_data: function (type){
        skills["data"] = type
        skills["period"] = 0
        skills.get();
    },
    change_selected_button: function (){
        let ids = ['week_button', 'month_button', 'year_button', 'all_button'];
        let data = {
            "daily": 'week',
            "weekly": 'month',
            "monthly": 'year',
            "all": 'all'
        };

        for (id in ids)
            if (ids[id] != 'skills_'+data[skills.data])
                skills[ids[id]].removeClass('active');
        skills[data[skills.data] + '_button'].addClass('active');
        skills.dropbox_button.text(skills[data[skills.data] + '_button'].text());
    },
    load: function (){
        skills.dropbox_button = $("#skills-dropbox");
        skills.week_button = $("#skills_week");
        skills.month_button = $("#skills_month");
        skills.year_button = $("#skills_year");
        skills.all_button = $("#skills_all");
        skills.date_text = $("#skills-date");

        skills.get();
    },
    get: function (){
        $.get(`/skills?date=${skills["data"]}&period=${skills["period"]}`)
        .done(function(data) {
            /* str --to-> JSON */
            data = JSON.parse(data);
            /* remove and reset canvas */
            $("#skills_chart").remove();
            $(".chart-container-skills").append('<div id="skills_chart"></div>');

            var w = window.innerWidth;
            if (w <= 655){
                data['mobile_labels'] = data['labels'];
                data['labels'] = [];
            }

            DrawApexRadarPolygon(data, "skills_chart");
            skills.date_text.text(data['date']);
        });
        skills.change_selected_button()
    }
};


window.onload = function () {
    sparring.load();
    feelings.load();
    attendance.load();
    skills.load();
    sparring_avg.load();
}
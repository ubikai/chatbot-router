let alertSystem = {
    init: function () {
        this.element = $(".info-container");
        this.contentElement = $(".info-alert");
        this.displayingAlert = false;
    },
    show: function (alertText) {
        if (!this.displayingAlert) {
            this.displayingAlert = true;
            this.contentElement.text(alertText);
            this.element.css("bottom", -this.element.height().toString() + "px");
            this.element.css("visibility", 'visible');
            this.element.css("transition", 'bottom 0.5s linear');
            this.element.css("bottom", "12px");
            setTimeout(function() {
                alertSystem.hide();
            }, 3000);
        }
    },
    hide: function () {
        this.element.css("transition", 'bottom 0.5s linear');
        this.element.css("bottom", "-20%");
        setTimeout(function() {
            alertSystem.element.css("visibility", 'hidden');
            alertSystem.contentElement.text("");
            alertSystem.displayingAlert = false
        }, 500);

    }
};

$( document ).ready(function() {
    alertSystem.init();
});
var link_container = {
    set: function(el){
        let select = el.options[el.options.selectedIndex];
        let page_id = select.dataset["page_id"];
        let cont_id = select.dataset["cont_id"];
        let data = {page_id: page_id, cont_id: cont_id};

        $.post("/admin", data);
        alertSystem.show("Request send.");
    }
}

function refreshFlows(cont_ip) {
    console.log(cont_ip);
    $.post("/refresh_flows", {cont_ip : cont_ip})
    .done(function( data ) {
        alert(data);
    });
}
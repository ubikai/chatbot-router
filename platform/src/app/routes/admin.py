from app.routes import *
import string
import random
import json
import re


@app.route("/admin", methods=['GET', 'POST'])
@login_required
def admin():
    if request.method == 'GET':
        out = requests.get("http://{}/manage_links".format(app.config["ROUTER_IP"]))
        if out:
            out = out.json()
        else:
            out = {"containers": [], "fb_pages": []}
        return render_template("admin.html", user=current_user, conts=out["containers"], pages=out["fb_pages"])
    elif request.method == 'POST':
        data = request.form.to_dict()
        if data:
            if data.get('cont_id') and data.get('page_id'):
                requests.post("http://{}/manage_links".format(app.config["ROUTER_IP"]), json=data)
                return "OK"
        return str(data)
    return "MNA"


@app.route("/refresh_flows", methods=['POST'])
@login_required
def refresh_flows():
    cont_ip = request.form.get('cont_ip')
    dprint('cont_ip=', cont_ip)
    if cont_ip:
        out = requests.get("http://" + cont_ip + "/refresh_flows")
        dprint('out=', out)
        if out:
            dprint('out.json()=', out.json())
            return out.json()['STATUS']


@app.route('/logs/<cont_ip>', methods=['GET'])
@login_required
def display_logs(cont_ip):
    if cont_ip:
        out = requests.get("http://{}/logs".format(cont_ip))
        logs = None
        if out:
            logs = out.json().get('LOGS').replace('[wsgi:error]', '')
            logs = re.sub('.pid\s.*:\w+.', '', logs)
            logs = logs.split('\n')[::-1]
        return render_template("logs.html", user=current_user, logs=logs)
    return redirect('/')

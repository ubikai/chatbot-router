from sys import path

path.append('/platform/src/')

from app.routes import *
import random
import string


def random_string(stringLength):
    return ''.join(random.choice(string.ascii_letters) for i in range(stringLength))


# noinspection PyArgumentList
def create_keywords():
    d = Data(name="first_name")
    d.submit()
    d = Data(name="last_name")
    d.submit()
    d = Data(name="profile_pic")
    d.submit()


# noinspection PyArgumentList
def create_test_user():
    create_keywords()

    u = User(bot_id=1, email="adm1n@ubik.chat", access_level=1)
    u.set_password('M@|)@|_|C|_|P!@c@')

    u.data.append(UserData(data=Data.get_data("first_name"), value="DELETE"))
    u.data.append(UserData(data=Data.get_data("last_name"), value="ME"))

    u.submit()
    print('User created')



if __name__ == '__main__':
    create_test_user()  # create 1 user
    # create_test_users()  # create x users (hardcoded)
    pass
